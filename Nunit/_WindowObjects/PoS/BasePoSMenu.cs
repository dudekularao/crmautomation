﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.PoS
{
    [Title("Select Item From")]
    [Title("Operator")]
    [Title("Department")]
    public class BasePoSMenu : WindowBase
    {
        public BasePoSMenu(Window window, ScreenRepository screenRepository) : base(window, screenRepository) { }

        [AutomationId("26683")]
        public ListBox SelectItem;

        [AutomationId("26684")]
        public TextBox SelectUserLogin;

        [ByName("Select default department")]
        public TextBox SelectDepartment;

        [ByName("OK")]
        public Button OK;

        [ByName("Cancel")]
        public Button Cancel;

        public virtual void SelectItemFromMenu(string menuItem = "#2")
        {
            SelectItem.Select2(menuItem);
            OK.Click();
            HandleDialog("Confirm", "Continue");
        }

        public virtual void SelectOperatorLogin(string userId = "")
        {
            if ( ! string.IsNullOrEmpty(userId))
            {
                int counter = 0;
                while ( ! SelectUserLogin.Text.Equals(userId) )
                {
                    SelectUserLogin.DoubleClick();
                    TestStack.White.InputDevices.Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.BACKSPACE);
                    TestStack.White.InputDevices.Keyboard.Instance.LeaveKey(KeyboardInput.SpecialKeys.BACKSPACE);
                    TestStack.White.InputDevices.Keyboard.Instance.Enter(userId);

                    if (counter > 3)
                        break;
                    counter++;
                }
                this.AddScreenshotToReport("User Selected " + SelectUserLogin.Text);
            }

            Get<BasePoSMenu>().OK.Click();
           
        }

        public virtual void SelectDefaultDept(string deptCode = "W")
        {
            var deptWind = Get<BasePoSMenu>("Department");
            if (! string.IsNullOrEmpty(deptCode))
            {
                deptWind.SelectDepartment.Click();
                deptWind.SelectDepartment.TypeText(deptCode);
            }
            deptWind.AddScreenshotToReport("Department selected");
            deptWind.OK.Click();
        }

    }
}
