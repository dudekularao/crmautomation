﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;
using System.Windows.Input;

namespace REV8_Autoline._WindowObjects.PoS
{
    [Title("Point Of Sale")]
    [Title("RTS Records")]
    [Title("RETURN")]
    [Title("Labour Details")]
    [Title("Action For Parts Retail Cash sales")]
    [Title("Payment Detail")]
    [Title("Complete Service Code/Mileage")]
    [Title("Workshop Launch Control")]
    [Title("Check In/Out")]
    [Title("Car bay")]
    public class PoSInvoiceWindow : WindowBase
    {
        public PoSInvoiceWindow(Window window, ScreenRepository screenRepository) : base(window, screenRepository) { }

        [AutomationId("26692")]
        public Panel ProductSearchBox;

        [AutomationId("26693")]
        public TextBox SelectCompany;

        [AutomationId("Vehicle")]
        public TabPage tbiVehicle;

        [AutomationId("26734")]
        public TextBox txtTimeTaken;

        [ByName("Technician number")]
        public TextBox TechnicianNme;

        [ByHelpText("dgo_ObjectID=menuSearch;")]
        public MenuBar MenuSearch;

        [ByName("26781")]
        public ListItem Labour;

        [AutomationId("26683")]
        public TextBox txtServiceCode;

        [AutomationId("SO_la_LOADSTAT")]
        public TextBox txtWorkStatus;

        [ByHelpText("editSearch")]
        public TextBox txtRTSCodeSearch;

        [ByName("Sale code")]
        public TextBox txtSaleCode;

        [ByName("26735")]
        public Button btnClocking;

        [AutomationId("26688")]
        public TextBox txtReference;

        [ByName("Payment code")]
        public TextBox txtPaymentCode;

        [AutomationId("26685")]
        public TextBox txtPreviousMileage;

        [AutomationId("26686")]
        public TextBox txtCurrentMileage;

        [AutomationId("26684")]
        public TextBox txtMileageIn;

        [ByName("RTS code")]
        public Button btnRtsCode;

        [ByName("OK")]
        public Button btnOK;

        [ByName("Cancel")]
        public Button btnCancel;

        [ByName("Save")]
        public Button btnSave;

        [ByName("Action")]
        public Button btnAction;

        [ByName("Last WIP")]
        public Button btnLastWip;

        [ByName("Payment")]
        public Button btnPayment;

        [ByName("Print")]
        public Button btnPrint;

        [ByName("Workshop launch control")]
        public Button btnWorkShopLaunchControl;

        [ByName("Check In/Out")]
        public Button btnCheckInOut;

        [ByName("Take possession of vehicle")]
        public RadioButton rbtnTakeVehiclePoss;

        [ByName("Return vehicle to customer")]
        public RadioButton rbtnReturnVehicleCust;

        [ByName("Vehicle")]
        public TabPage tabVehicle;

        [ByHelpText("MK_VE_LASTMLG")]
        public Label txtPrevious;

        [ByName("Current")]
        public TextBox txtCurrent;

        public virtual void AddProduct(string CompanyName = "")
        {
            SelectCompany.DoubleClick();
            Wait(1000);
            TestStack.White.InputDevices.Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.BACKSPACE);
            TestStack.White.InputDevices.Keyboard.Instance.LeaveKey(KeyboardInput.SpecialKeys.BACKSPACE);
            TestStack.White.InputDevices.Keyboard.Instance.Enter(CompanyName);
            SelectCompany.KeyIn(KeyboardInput.SpecialKeys.TAB);
            Wait(1000);
            ProductSearchBox.ClickOnElipsis();
            Wait(5000);
            //MenuSearch.Select();

        }

        public virtual void AddTechnician(string TechnicianName = "", String TechnicianNumber = "", String WorkStatus = "", String TimeTaken = "")
        {
            WaitForWindow("RTS Records");
            txtRTSCodeSearch.SetText(TechnicianNumber);
            //TestStack.White.InputDevices.Keyboard.Instance.Enter(TechnicianNumber);
            btnRtsCode.Click();
            Wait(1000);
            btnOK.Click();
            WaitForWindow("Labour Details");
            TechnicianNme.SetText(TechnicianName);
            txtTimeTaken.Click();
            TestStack.White.InputDevices.Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.END);
            TestStack.White.InputDevices.Keyboard.Instance.LeaveKey(KeyboardInput.SpecialKeys.BACKSPACE);
            TestStack.White.InputDevices.Keyboard.Instance.LeaveKey(KeyboardInput.SpecialKeys.BACKSPACE);
            txtTimeTaken.SetText(TimeTaken);
            txtWorkStatus.SetText(WorkStatus);
            Wait(5000);
            var LDetailsWind = Get<PoSInvoiceWindow>("Labour Details");
            LDetailsWind.btnOK.Click();

        }

        public virtual void GetPreviousVehicleMileage()
        {
            tabVehicle.Click();
            var LDetailsWind = Get<PoSInvoiceWindow>("Point Of Sale");
            int VehiclePreviousMileage = int.Parse(LDetailsWind.txtPrevious.Value);
            //Console.WriteLine(VehiclePreviousMileage);
            //int MillageReading = VehiclePreviousMileage + 1;
            String PreviousMileage = VehiclePreviousMileage.ToString();
            btnWorkShopLaunchControl.Click();
            var WorkShopWind = Get<PoSInvoiceWindow>("Workshop Launch Control");
            WorkShopWind.btnCheckInOut.Click();
            var CheckInOutWind = Get<PoSInvoiceWindow>("Check In/Out");
            CheckInOutWind.rbtnTakeVehiclePoss.Click();
            CheckInOutWind.txtMileageIn.SetText(PreviousMileage);
            CheckInOutWind.btnOK.Click();
            var CarBayWind = Get<PoSInvoiceWindow>("Car bay");
            CarBayWind.btnOK.Click();
            Wait(5000);
            //btnSave.Click();
            //var ReturnWind = Get<PoSInvoiceWindow>("RETURN");
            //ReturnWind.btnOK.Click();



        }

        public virtual void PrintInvoice(String Reference = "", String PaymentCode = "", String ServiceCode = "")
        {

            //btnLastWip.Click();
            WaitForWindow("Point Of Sale");
            btnPrint.Click();
            WaitForWindow("Action For Service Retail Cash Sales");
            var printInvoiceWind = Get<PoSInvoiceWindow>("Action For Service Retail Cash Sales");
            printInvoiceWind.btnPayment.Click();
            WaitForWindow("Payment Detail");
            var paymentWind = Get<PoSInvoiceWindow>("Payment Detail");
            paymentWind.txtReference.SetText(Reference);
            paymentWind.txtPaymentCode.SetText(PaymentCode);
            paymentWind.btnOK.Click();
            WaitForWindow("Action For Service Retail Cash Sales");
            printInvoiceWind = Get<PoSInvoiceWindow>("Action For Service Retail Cash Sales");
            printInvoiceWind.btnPrint.Click();
            WaitForWindow("Complete Service Code");
            var ServiceCodeWind = Get<PoSInvoiceWindow>("Complete Service Code");
            ServiceCodeWind.txtServiceCode.Click();
            TestStack.White.InputDevices.Keyboard.Instance.LeaveKey(KeyboardInput.SpecialKeys.BACKSPACE);
            ServiceCodeWind.txtServiceCode.SetText(ServiceCode);
            SelectCompany.KeyIn(KeyboardInput.SpecialKeys.TAB);
            var ReturnWind = Get<PoSInvoiceWindow>("RETURN");
            ReturnWind.btnOK.Click();


        }
    }
}






