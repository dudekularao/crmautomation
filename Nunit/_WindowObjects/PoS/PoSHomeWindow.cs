﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.PoS
{
    [Title("Point Of Sale")]
    public class PoSHomeWindow : WindowBase
    {
        public PoSHomeWindow(Window window, ScreenRepository screenRepository) : base(window, screenRepository) { }

        [ByName("WIP number")]
        public TextBox WIPnumber;

        [AutomationId("26686")]
        public TextBox CustomerSearch;

        public virtual bool SearchWIPNumber(string WIPnum = "")
        {
            WIPnumber.SetText2(WIPnum);
            WIPnumber.KeyIn(KeyboardInput.SpecialKeys.TAB);

            string errorText = GetTextHandleDialog("RETURN   (Cancel to Break)", "OK");

            if (string.IsNullOrEmpty(errorText))
            {
                ExtentBase.LogPassStep($"{WIPnum} loaded successfully");
                return true;
            }
            else
                ExtentBase.LogFailStep(errorText, addScreenShot:false);
            return false;
        }

        public virtual void SearchCustomerEnterText(string customerName = "")
        {
            CustomerSearch.SetText2(customerName);
            WIPnumber.KeyIn(KeyboardInput.SpecialKeys.TAB);
        }
    }


}
