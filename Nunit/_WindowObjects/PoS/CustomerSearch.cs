﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.PoS
{
    [Title("Point Of Sale")]
    [Title("Customer")]
    public class CustomerSearch : WindowBase
    {
        public CustomerSearch(Window window, ScreenRepository screenRepository) : base(window, screenRepository) { }

        [AutomationId("26686")]
        public Panel CustomerSearchBox;

        [AutomationId("26684")]
        public TextBox TextToLocateCustomer;

        [AutomationId("27015")]
        public TextBox VehichleIdentity;

        [AutomationId("26688")]
        public Table CustomersSearchTable;

        [AutomationId("26697")]
        public Table VehicleSearchTable;

        [ByName("OK")]
        public Button OkButton;

        [ByName("No")]
        public Button NoButton;

        [ByName("Cancel")]
        public Button CancelButton;

        [ByName("Continue")]
        public Button ContinueButton;

        [ByName("Search")]
        public Button SearchButton;


        public virtual bool SearchCustomer(string customerName = "")
        {
            //Console.WriteLine(CustomerSearchBox.GetElement(SearchCriteria.ByClassName("KCMLEdit32")).GetRuntimeId());
            //CustomerSearchBox.GetElement(SearchCriteria.ByClassName("KCMLEdit32")).KeyIn(KeyboardInput.SpecialKeys.TAB);
            try
            {
                CustomerSearchBox.ClickOnElipsis();
                WaitForWindow("Customer Word");
                TextToLocateCustomer.SetText2(customerName);
                SearchButton.Click();
            }
            catch (Exception) {
                return false;
            }

             if( CustomersSearchTable.GetRowCount() <= 0 )
            {
                ExtentBase.LogFailStep($"There are no records with the given customer: {customerName}");
                return false;
            }
            var custName = CustomersSearchTable.GetCellData(2, 1);
            CustomersSearchTable.ClickOnCell(2, 1);

            if ( VehicleSearchTable.GetRowCount() <= 1 )
            {
                ExtentBase.LogFailStep($"There are no vehicles with the given customer: {customerName}");
                return false;
            }
            VehicleSearchTable.ClickOnCell(3, 1);
            OkButton.Click();

            //CustomerSearch window = Get<CustomerSearch>("Overwrite Customer");
            //window.NoButton.Click();

            HandleDialog("Overwrite Customer", "No");

            CustomerSearch window = Get<CustomerSearch>("Other Jobs Exist");
            window.CancelButton.Click();

            window = Get<CustomerSearch>(custName);
            window.CancelButton.Click();

            return true;
        }


    }
}
