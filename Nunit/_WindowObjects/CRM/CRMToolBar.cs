﻿using System;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.CRM
{
    [Title("MS Test pack")]
    [Title("CRM")]
    public class CRMToolBar : WindowBase
    {
        public CRMToolBar(Window window, ScreenRepository screenRepository)
            : base(window, screenRepository) { }

        [ByName("Save")]
        public Button Save;

        [ByName("Exit")]
        public Button Exit;
        
        [ByName("Edit")]
        public Button Edit;

        [ByName("Close")]
        public Button CloseBtn;

        [ByName("Open")]
        public Button Open;

        public virtual void ClickSave()
        {
            Get<CRMToolBar>().Save.Click();
            WaitForExist(3000);
        }
        
    }
}
