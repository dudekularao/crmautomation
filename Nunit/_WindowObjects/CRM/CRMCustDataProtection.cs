﻿using System;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.CRM
{
    [Title("CRM")]
    [Title("Search")]
    [Title("Create a new Data Protection Agreement")]
    class CRMCustDataProtection : WindowBase
    {
        public CRMCustDataProtection(Window window, ScreenRepository screenRepository)
           : base(window, screenRepository) { }

        [ByName("Data protection")]
        public TabPage DataProtection;

        [AutomationId("27948")]
        public Table AgreementsTable;

        [AutomationId("26683")]
        public TextBox AgreementDescription;

        [AutomationId("26686")]
        public CheckBox DefaultOrUse;

        [AutomationId("26724")]
        public TextBox StatementValidFrom;

        [AutomationId("26729")]
        public TextBox StatementValidUpto;

        [AutomationId("26693")]
        public TextBox OEMNotes;

        [AutomationId("26764")]
        public TextBox DealerNotes;

        [ByName("Save")]
        public Button SaveBtn;

        [ByName("Close")]
        public Button CloseBtn;


        public virtual void GoToAgreementsTab(string custMagicNo = "")
        {
            GetWindow<CRMCustomerWindow>().GoToAgreementsTab(custMagicNo);
        }

        public virtual void RightClickOnAgreementsTable()
        {
            AgreementsTable.RightClick();
            WaitForExist(1000);
        }

        public virtual void AddAgreement(string aggrDesc, string validFrom, string validUpto, string custMagicNo = "")
        {
            GoToAgreementsTab(custMagicNo);
            RightClickOnAgreementsTable();
            TestStack.White.InputDevices.Keyboard.Instance.Enter("C");
            WaitForWindow("Create",60);

            if (!WindowTitle.Contains("Create"))
                return;

            AgreementDescription.SetText2(aggrDesc);
            DefaultOrUse.Click();
            StatementValidFrom.SetText2(validFrom);
            StatementValidUpto.SetText2(validUpto);

            OEMNotes.SetText2("Test");
            DealerNotes.SetText2("DealerNote");

            if( SaveBtn.Enabled)
            {
                SaveBtn.Click();
                Wait(1, 5);
                //wasn't tested
                ExtentBase.LogPassStep("Agreement Added");
            }
            else
                ExtentBase.LogFailStep("Save button isn't enabled");

            CloseBtn.Click();
            return;
        }
    }
}
