﻿using System;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.CRM
{
    [Title("CRM")]
    [Title("Search")]
    class CRMCustomerWindow : WindowBase
    {
        public CRMCustomerWindow(Window window, ScreenRepository screenRepository)
           : base(window, screenRepository) { }

        [ByName("Find record")]
        public TextBox Findrecord;

        [AutomationId("26764")]
        public TextBox SearchRecord;

        [ByName("Create")]
        public Button CreateButton;

        [ByHelpText("MK_TA_FIRSTNAM0")]
        public TextBox FirstName;

        [ByHelpText("MK_TA_SURNAME0")]
        public TextBox SurName;

        [ByHelpText("MK_TA_EMAIL0")]
        public TextBox EmailID;

        [ByHelpText("MK_TA_PHONE_4")]
        public TextBox MobilePhoneNum;

        [ByHelpText("MK_TA_PREFPHON")]
        public TextBox PrefContact;

        [ByName("Additional Details")]
        public Tab AdlDetailsTab;

        [ByName("Job title")]
        public TextBox JobTitle;

        [ByName("Gender")]
        public TextBox Gender;

        [ByHelpText("Menu..list")]
        public ListItems CustBranchList;

        [ByName("Continue")]
        public Button ContinueBtn;

        [ByName("OK")]
        public Button OkButton;

        [ByName("Data protection")]
        public TabPage DataProtection;

        public virtual string CreateCustomer(string recordName="Test", string firstName="TestCustomer", 
                                            string lastName = "lastName",string mobile = "123456789", 
                                            string email="abc@cdk.com", string job="Actor", string prefCont = "3",
                                            string gender ="M", string CustBranch = "#1")
        {
            if (recordName.Equals("Test"))
            {
                recordName = recordName + DateTime.Now.ToString("yyyyMMddHHMMss");
            }
            Findrecord.SetText2(recordName);
            Findrecord.KeyIn(KeyboardInput.SpecialKeys.TAB);
            WaitForExist(3000);
            CreateButton.Click();
            WaitForExist(3000);
            TestStack.White.InputDevices.Keyboard.Instance.Enter("u");
            WaitForExist(3000);
            CustBranchList.Select2(CustBranch);
            OkButton.Click();
            FirstName.SetText2(firstName);
            if (lastName.Equals("lastName"))
            {
                var tempMagNo = GetWindow<CRMWindow>().statusBar.Name;
                lastName = lastName + tempMagNo.Substring(tempMagNo.Length - 4);
            }                
            SurName.SetText(lastName);
            MobilePhoneNum.SetText(mobile);
            EmailID.SetText(email);
            PrefContact.SetText(prefCont);
            KcmlTab.SelectTab("Additional Details");
            JobTitle.SetText(job);
            Gender.SetText(gender);
            Get<CRMToolBar>().Save.Click();
            WaitForExist(3000);

            return GetWindow<CRMWindow>().statusBar.Name;
        }

        public virtual void LinkCustomerWithCompany(string customerMglNo, string companyMglNo)
        {
            CRMWindow crmWindow = Get<CRMWindow>();
            crmWindow.GoToCustomer();
            crmWindow.FindRecordInCRM(customerMglNo);

            Get<CRMToolBar>().Edit.Click();
            WaitForExist(3000);

            crmWindow = Get<CRMWindow>();
            crmWindow.GoToCompany();
            WaitForExist(3000);
            TestStack.White.InputDevices.Keyboard.Instance.Enter(companyMglNo);
            TestStack.White.InputDevices.Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.TAB);

            //SearchRecord.SetText(companyMglNo);
            //SearchRecord.KeyIn(KeyboardInput.SpecialKeys.TAB);

            // Get<CRMToolBar>().Save.Click();
        }

        public virtual void GoToAgreementsTab(string custMagicNo="")
        {
            if ( ! string.IsNullOrEmpty(custMagicNo.Trim()) )
            {
                CRMWindow crmWindow = Get<CRMWindow>();
                crmWindow.GoToCustomer();
                crmWindow.FindRecordInCRM(custMagicNo);
            }

            Get<CRMToolBar>().Edit.Click();
            WaitForExist(3000);

            DataProtection.Click();
            this.AddScreenshotToReport("Agreement page");
        }

        public virtual void AddAgreement(string custMagicNo="")
        {
            GoToAgreementsTab(custMagicNo);

        }
    }
}
