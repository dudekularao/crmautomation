﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.CRM
{
    [Title("CRM")]
    [Title("Search")]
    [Title("Select Item From")]
    public class CRMWindow : WindowBase
    {
        public CRMWindow(Window window, ScreenRepository screenRepository) : base(window, screenRepository) { }

        [ByName("dgo_ObjectID=gridTaskBar;")]
        public Table CRMshortcuts;

        [ByName("Find record")]
        public TextBox Findrecord;

        [ByName("Create")]
        public Button createBtn;

        [ByName("Customer")]
        public Button customerOption;

        [ByName("Save")]
        public Button saveBtnCRM;

        [ByName("OK")]
        public Button okBtnLocation;

        [ByName("Continue")]
        public Button ContinueBtn;


        [AutomationId("StatusBar.Pane1")]
        public UIItem statusBar;

        [AutomationId("100")]
        public Table table;

        public virtual void GoToCustomer()
        {
            CRMshortcuts.ClickOnCell(10, 2);
            WaitForExist(3000);
        }

        public virtual void SelectTableItem(string tableItem)
        {
            Wait();
        }

        public virtual void GoToCompany()
        {
            CRMshortcuts.ClickOnCell(7, 2);
            WaitForExist(3000);
        }

        public virtual void GoToVehicle()
        {
            CRMshortcuts.ClickOnCell(13, 2);
            WaitForExist(3000);
        }

        public virtual bool FindRecordInCRM(string recordMagicalNo)
        {
            WaitForExist(3000);
            Findrecord.SetText2(recordMagicalNo);
            Findrecord.KeyIn(KeyboardInput.SpecialKeys.TAB);
            WaitForExist(3000);
            if(ContinueBtn.Enabled)
                ContinueBtn.Click();
            WaitForExist(3000);

            if (statusBar.Name.Contains(recordMagicalNo))
            {
                this.AddScreenshotToReport("Record Found : " + statusBar.Name);
                return true;
            }
            else
                this.AddScreenshotToReport("There's no record found with magic number : " + recordMagicalNo, AventStack.ExtentReports.Status.Fail);
            return false;
        }

    }

    public class SearchCRMWindow : WindowBase
    {
        public SearchCRMWindow(Window window, ScreenRepository screenRepository)
                : base(window, screenRepository) { }

        [ByHelpText("dgo_ObjectID=editSearch;")]
        public TextBox searchFor;

    }
}

