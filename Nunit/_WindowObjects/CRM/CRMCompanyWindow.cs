﻿using System;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.CRM
{
    class CRMCompanyWindow : WindowBase
    {
        public CRMCompanyWindow(Window window, ScreenRepository screenRepository)
           : base(window, screenRepository) { }

        [ByName("Find record")]
        public TextBox Findrecord;

        [ByName("Create")]
        public Button createBtn;

        [ByHelpText("editCompanyName")]
        public TextBox companyName;

        [ByHelpText("MK_CO_ADDRESS0")]
        public TextBox addressTextArea;

        [ByHelpText("MK_CO_POSTCODE0")]
        public TextBox postalCde;

        [ByHelpText("MK_CO_PHONE_2")]
        public TextBox otherPhone;

        [ByHelpText("dbStatus")]
        public TextBox statusDDvalue;

        /// <summary>
        /// This method will never be used as part of current functionality.
        /// </summary>
        public virtual void CreateCompany()
        {
            Findrecord.SetText2("Test");
            Findrecord.KeyIn(KeyboardInput.SpecialKeys.TAB);
            WaitForExist(3000);
            createBtn.Click();
            WaitForExist(3000);
            TestStack.White.InputDevices.Keyboard.Instance.Enter("C");
            WaitForExist(3000);
            companyName.SetText2("Corollco");
            WaitForExist(3000);
            addressTextArea.SetText2("LA,California");
            WaitForExist(3000);
            postalCde.SetText2("9875452");
            WaitForExist(3000);
            otherPhone.SetText2("789764564");
            WaitForExist(3000);
            statusDDvalue.SetText2("L");
            WaitForExist(3000);
            Get<CRMToolBar>().Save.Click();
            WaitForExist(3000);
        }
    }
}
