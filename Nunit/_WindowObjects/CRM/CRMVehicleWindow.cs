﻿using System;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline._WindowObjects.CRM
{
    [Title("CRM - MS Test pack")]
    [Title("Search")]
    public class CRMVehicleWindow : WindowBase
    {
        public CRMVehicleWindow(Window window, ScreenRepository screenRepository)
           : base(window, screenRepository) { }

        [ByName("Find record")]
        public TextBox Findrecord;

        [ByName("Create")]
        public Button CreateBtn;

        [ByHelpText("dbRegNo")]
        public TextBox RegistationNo;

        [ByHelpText("MK_VE_DDCO")]
        public TextBox DDCompany;

        [ByHelpText("dbFran")]
        public TextBox Franchise;

        [ByHelpText("dbModel")]
        public TextBox Model;

        [ByHelpText("dbVariant")]
        public TextBox Variant;

        [AutomationId("26764")]
        public TextBox SearchCustomerToLink;

        [AutomationId("27019")]
        public Table LinkCustomerPane;

        [ByName("Vehicle history")]
        public TabPage VehicleHistory;

        [AutomationId("27156")]
        public Table VehicleHistoryTable;

        //[ByClassName("KTabEars")]
        //public UIItemCollection PaneLinksColl;

        //[ByClassName("26698")]
        //public UIItemCollection PaneLinks;

        //[ByClassName("KTabEars")]
        //public Tab PaneLinks2;

        //[AutomationId("KTabEars.Pane3")]
        //public UIItem PaneLinks3;


        public virtual void CreateVehicle(string RecordName, string FranchiseCode, string ModelCode,
            string VariantCode, string DDCompanyNo = "00", string RegNo = "^^")
        {
            Findrecord.SetText2(RecordName);
            Findrecord.KeyIn(KeyboardInput.SpecialKeys.TAB);
            WaitForWindow("Search", 60);
            CreateBtn.Click();
            WaitForExist(3000);
            TestStack.White.InputDevices.Keyboard.Instance.Enter("V");
            WaitForExist(3000);

            HandleDialog("Select Item from List", "OK");
            WaitForExist(3000);

            RegistationNo.SetText2(RegNo);
            DDCompany.SetText2(DDCompanyNo);
            Franchise.SetText2(FranchiseCode);
            Model.SetText2(ModelCode);
            Variant.SetText2(VariantCode);
            Get<CRMToolBar>().Save.Click();
            WaitForExist(3000);

            //string dlgtext = GetTextHandleDialog("Reasons For Rejecting", "OK");
            //Console.WriteLine(dlgtext);

        }


        public virtual void LinkVehicleWithCustomer(string VMagicNo, string CustMagicNo)
        {
            CRMWindow crmWindow = Get<CRMWindow>();
            crmWindow.GoToVehicle();
            crmWindow = Get<CRMWindow>();
            crmWindow.FindRecordInCRM(VMagicNo);

            Get<CRMToolBar>().Edit.Click();
            WaitForExist(3000);

            LinkCustomerPane.ClickOnCell(1, 2);

            WaitForWindow("Search",60);
            TestStack.White.InputDevices.Keyboard.Instance.Enter(CustMagicNo);
            TestStack.White.InputDevices.Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.TAB);

            //SearchCustomerToLink.SetText(CustMagicNo); // This is not working
            //SearchCustomerToLink.KeyIn(KeyboardInput.SpecialKeys.TAB);

            // Get<CRMToolBar>().Save.Click();
        }


        public virtual void GoToVehicleHistory(string VMagicNo)
        {
            CRMWindow crmWindow = Get<CRMWindow>();
            crmWindow.GoToVehicle();
            crmWindow.FindRecordInCRM(VMagicNo);
            WaitForExist(3000);
            WindowTestBase.kcmlAppScreen = Get<CRMVehicleWindow>();
            //WindowTestBase.kcmlAppScreen.KcmlTable()
            //approach 1
            //var tabs = WindowTestBase.kcmlAppScreen.GetAllTabs();
            //tabs[0].Pages[4].Click();
            //approach 2
            VehicleHistory.Click();
            WaitForExist(1000);            
            ExtentBase.LogPassStep("History Rows Count : " +  VehicleHistoryTable.GetRowCount());
           // ExtentBase.LogPassStep(VehicleHistoryTable.GetRowValue(1));
            //ExtentBase.LogPassStep(VehicleHistoryTable.GetRowValue(2));


        }


    }
}
