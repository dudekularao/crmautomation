﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using REV8_Autoline.CoreFramework;
using REV8_Autoline._WindowObjects.CRM;
using NUnit.Framework;
using REV8_Autoline._WindowObjects.PoS;

namespace REV8_Autoline.Tests.PoS
{
    class PoSTests : WindowTestBase
    {
        [SetUp]
        public void SetUpPoS()
        {
            TestData = readExcel.FilterRows(TestContext.CurrentContext.Test.Name, "POS");
            LoginToKCML();
            string windowTitle = KCMLLoginIn_SelectCompanyModuleMenuItem("Point of sale",
                                     GetData("Company"), "Point-of-sale");
        }

        [Test, Category("PoS")]
        public void LoginToPoS()
        {
            var basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.AddScreenshotToReport("Logged into Point of sale");
        }

        [Test, Category("PoS")]
        public void SelectPartsOrService()
        {
            var basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectItemFromMenu(GetData("ItemParts"));
        }

        [Test, Category("PoS")]
        public void SelectOperatorAndDepartment()
        {
            SelectPartsOrService();

            var basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectOperatorLogin(GetData("Operator"));

            basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectDefaultDept(GetData("Department"));
        }

        [Test, Category("PoS")]
        public void SearchWIPNumber()
        {
            var wipNumber = GetData("WIPNumber");
            SelectOperatorAndDepartment();
            var posHome = GetWindow<PoSHomeWindow>();
            Assert.True(posHome.SearchWIPNumber(wipNumber));
        }

        [Test, Category("PoS")]
        public void SearchCustomer()
        {
            SelectOperatorAndDepartment();
            var customerSearch = GetWindow<CustomerSearch>();
            Assert.True(customerSearch.SearchCustomer("OJ Horner"));
        }

        [Test, Category("PoS")]
        public void SearchAccount()
        {
            SelectOperatorAndDepartment();
            var customerSearch = GetWindow<CustomerSearch>();
            // Assert.True(customerSearch.SearchCustomer("OJ Horner"));
        }

        [TearDown]
        public void tearDown()
        {
            //var basePoSMenu = GetWindow<BasePoSMenu>();
            //basePoSMenu.Cancel.Click();
        }

        [Test, Category("PoS")]
        public void PoSInvoice()
        {
            //Login to POS
            var basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectItemFromMenu(GetData("ItemParts"));

            //Select Department and Search Customer
            basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectOperatorLogin(GetData("Operator"));

            basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectDefaultDept(GetData("Department"));

            var customerSearch = GetWindow<CustomerSearch>();
            Assert.True(customerSearch.SearchCustomer("OJ Horner"));



            //Add Labour from New Product
            var PoSInvoiceWindow = GetWindow<PoSInvoiceWindow>();
            PoSInvoiceWindow.AddProduct(GetData("CompanyName"));
            PoSInvoiceWindow.AddTechnician(GetData("TechnicianName"), GetData("TechnicianNumber"));
            PoSInvoiceWindow.GetPreviousVehicleMileage();
            basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectOperatorLogin(GetData("Operator"));
            basePoSMenu = GetWindow<BasePoSMenu>();
            basePoSMenu.SelectDefaultDept(GetData("Department"));
            PoSInvoiceWindow = GetWindow<PoSInvoiceWindow>();
            PoSInvoiceWindow.PrintInvoice(GetData("Reference"), GetData("PaymentCode"));


        }
    }
}
