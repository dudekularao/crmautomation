﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestStack.White;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.ScreenObjects;
using System.Configuration;
using IVCDriveTests.Common;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;
using System.Threading;
using System.Net;
using System.Web;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems;
using REV8_Autoline.CoreFramework;
using REV8_Autoline._WindowObjects.CRM;
using System.Data;

namespace REV8_Autoline
{
    public class ModuleTest_CRM : WindowTestBase
    {
        [SetUp]
        [Test, Category("CRM")]
        public void LoginToCRM()
        {
            TestData = readExcel.FilterRows(TestContext.CurrentContext.Test.Name, "CRM");
            LoginToKCML();
            string windowTitle = KCMLLoginIn_SelectCompanyModuleMenuItem(GetCommonData("SelectCompany")
                                     , GetCommonData("SelectModule"), GetCommonData("SelectMenuItem"));
            Assert.IsTrue(windowTitle.Contains("CRM"), "CRM Pack wasn't  loaded. Please verify the Company and Module details.");

            kcmlAppScreen = GetWindow<CRMWindow>();
            kcmlAppScreen.AddScreenshotToReport("Logged into CRM");
            // ScreenShotUtils.AddScreenshotToReport("WindowBase");
            // ExtentBase.LogPassStep("Screenshot", ScreenShotUtils.captureScreenShotAsBase64($"{path}.jpg"));
            // ScreenShotUtils.AddScreenshotToReport(Get<CRMWindow>(),"generic"); ;
        }

        [Test, Category("CRM")]
        public void SearchCustomerInCRM()
        {
            var num =GetData("CustMagicNo");
            CRMWindow crmWindow = GetWindow<CRMWindow>();
            crmWindow.GoToCustomer();
            crmWindow = GetWindow<CRMWindow>();
            Assert.True(crmWindow.FindRecordInCRM(num), "Given record wasn't found. Please give the corret magic number");
        }     

        [Test, Category("CRM")]
        public void SearchCompanyInCRM()
        {
            var num = GetData("CompMagicNo");
            CRMWindow crmWindow = GetWindow<CRMWindow>();
            crmWindow.GoToCompany();
            Assert.True(crmWindow.FindRecordInCRM(num), "Given record wasn't found. Please give the corret magic number");
        }

        [Test, Category("CRM")]
        public void SearchVehicleInCRM()
        {
            var num = GetData("VMagicNo");
            CRMWindow crmWindow = GetWindow<CRMWindow>();
            crmWindow.GoToVehicle();
            Assert.True(crmWindow.FindRecordInCRM(num), "Given record wasn't found. Please give the corret magic number");
        }

        [Test]
        public void CreateCustomer()
        {
            CRMCustomerWindow crmCustWindow = GetWindow<CRMCustomerWindow>();
            ExtentBase.LogPassStep("Customer created with magical No; :" + crmCustWindow.CreateCustomer());
        }

        [Test]
        public void LinkCustomerWithCompany()
        {
            var companyMagicNo = GetData("CompMagicNo");
            var custMagicNo = GetData("CustMagicNo");
            CRMCustomerWindow crmCustWindow = GetWindow<CRMCustomerWindow>();
            crmCustWindow.LinkCustomerWithCompany(custMagicNo, companyMagicNo);
        }

        [Test]
        public void AddAgreement()
        {
            var custMagicNo = GetData("CustMagicNo");
            CRMCustomerWindow crmCustWindow = GetWindow<CRMCustomerWindow>();
            crmCustWindow.AddAgreement(custMagicNo);
        }

        [Test]
        public void DeleteAgreement()
        {
            var custMagicNo = GetData("CustMagicNo");
            CRMCustomerWindow crmCustWindow = GetWindow<CRMCustomerWindow>();
            crmCustWindow.AddAgreement(custMagicNo);
        }

        [Test]
        public void CreateVehicle()
        {
            CRMVehicleWindow createVehicleWindow = GetWindow<CRMVehicleWindow>();
            createVehicleWindow.CreateVehicle(GetData("Vname"), GetData("Franchise")
                                        , GetData("Model"), GetData("Variant"));
        }

        [Test]
        public void LinkVehicleWithCustomer()
        {
            var VMagicNo = GetData("VMagicNo");
            var CustMagicNo = GetData("CustMagicNo");
            CRMVehicleWindow createVehicleWindow = GetWindow<CRMVehicleWindow>();
            createVehicleWindow.LinkVehicleWithCustomer(VMagicNo, CustMagicNo);
        }

        [Test]
        public void GoToVehicleHistory()
        {
            var VMagicNo = GetData("VMagicNo");
            CRMVehicleWindow createVehicleWindow = GetWindow<CRMVehicleWindow>();
            createVehicleWindow.GoToVehicleHistory(VMagicNo);
        }


    }

}
