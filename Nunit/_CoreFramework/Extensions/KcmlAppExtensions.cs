﻿using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.ScreenObjects;

namespace REV8_Autoline.CoreFramework
{
    public static class KcmlAppExtensions
    {
        public static void AddScreenshotToReport(this KcmlAppScreen currentWindow, string stepName = "Image", Status status = Status.Pass)
        {
            string htmlBase64 = string.Empty;
            if (ConfigSetUp.TurnOffAllScreenshots.ToLower().Equals("false"))
            {
                string screenshotFolder = Path.Combine(ExtentBase.extentReportPath, "Screenshots");
                if (!Directory.Exists(screenshotFolder))
                    Directory.CreateDirectory(screenshotFolder);
                string path = $"{Path.Combine(ExtentBase.extentReportPath, "Screenshots")}/{DateTime.Now.ToString("yyyyMMddHHMMss")}";
                try
                {
                    currentWindow.TakeScreenshot(path);
                    byte[] imageArray = File.ReadAllBytes($"{path}.jpg");
                    string base64 = Convert.ToBase64String(imageArray);
                    htmlBase64 = string.Format("<br/><a href='data:image/gif;base64,{0}' data-featherlight='image'><img style='width:250px' class='report-img' src='data:image/gif;base64,{0}' alt=Click on the image/></a>", base64);
                }
                catch (Exception)
                {
                    ExtentBase.GetTargetTest().Warning($"Unable to capture for step :{stepName}");
                    return;
                }
            }
            switch (status)
            {
                case Status.Pass:
                    ExtentBase.GetTargetTest().Pass($"{stepName} \n {htmlBase64}");
                    break;
                case Status.Fail:
                    ExtentBase.GetTargetTest().Fail($"{stepName} \n {htmlBase64}");
                    break;
            }
        }
    }
}
