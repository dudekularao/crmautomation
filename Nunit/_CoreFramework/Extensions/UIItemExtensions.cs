﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;

namespace REV8_Autoline.CoreFramework
{
    public static class UIItemExtensions
    {

        public static void Select2(this ListBox listBox, string selectItem)
        {
            if (selectItem.Equals("^^"))
                return;
            try
            {
                selectItem = selectItem.Trim();
                if (selectItem.StartsWith("#"))
                {
                    int selectIndex = Int32.Parse(selectItem.Replace("#", string.Empty));
                    listBox.Select(selectIndex);
                    return;
                }
                if (selectItem.Contains("*"))
                {
                    try
                    {
                        listBox.Select(selectItem, false);
                    }
                    catch (Exception)
                    {
                        ExtentBase.LogFailStep($"Unable to Find list item contains text {selectItem}");
                    }
                    return;
                }
                listBox.Select(selectItem);
            }
            catch (Exception)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(1).GetMethod().Name;
                var window = stackTrace.GetFrame(1).GetMethod().DeclaringType.Name;
                ExtentBase.LogFailStep($"Unable to Select {selectItem} from {method} in window: {window}");
            }
        }

        public static void Select2(this ListItems listBox, string selectItem)
        {
            if (selectItem.Equals("^^"))
                return;
            try
            {
                selectItem = selectItem.Trim();
                if (selectItem.StartsWith("#"))
                {
                    int selectIndex = Int32.Parse(selectItem.Replace("#", string.Empty));
                    listBox.Select(selectIndex);
                    return;
                }
                if (selectItem.Contains("*"))
                {
                    try
                    {
                        listBox.Item(selectItem, false).Select();
                    }
                    catch (Exception)
                    {
                        ExtentBase.LogFailStep($"Unable to Find list item contains text {selectItem}");
                    }
                    return;
                }
                listBox.Select(selectItem);
            }
            catch (Exception)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(1).GetMethod().Name;
                var window = stackTrace.GetFrame(1).GetMethod().DeclaringType.Name;
                ExtentBase.LogFailStep($"Unable to SetText from {method} in window: {window}");
            }
        }

        public static void SetText2(this TextBox textBox, string inputText, bool clear=false)
        {

            if (inputText.Equals("^^"))
                return;
            try
            {
               textBox.SetText(inputText,clear);
            }
            catch (AutomationException ae)
            {
                ExtentBase.LogFailStep($"Unalbe to identify the text box, \n error message : {ae.Message}");
            }
            catch (ElementNotAvailableException ae)
            {
                ExtentBase.LogFailStep($"Unalbe to identify the text box, Intialize the window. \n error message : {ae.Message}");
            }
            catch (Exception)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(1).GetMethod().Name;
                var window = stackTrace.GetFrame(1).GetMethod().DeclaringType.Name;
                ExtentBase.LogFailStep($"Unable to SetText from {method} in window: {window}");
            }
        }

        public static void SetText2(this TextBox textBox, string inputText, bool mandatory , bool clear= false, string FiledName = "")
        {
            if (mandatory)
            {
                if (inputText.Equals("^^") || string.IsNullOrEmpty(inputText))
                {
                    throw new InvalidTestDataException($"Test data is mandatory for this filed: {FiledName}");
                }
            }

            try
            {
                textBox.SetText(inputText, clear);

            }
            catch (Exception)
            {
                //fail log for textbox to identify the failed text box
            }
        }
    }
}

