﻿using ExcelDataReader;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REV8_Autoline.CoreFramework
{
    public class ReadExcel
    {
        public ReadExcel()
        {
            try
            {
                LoadExcel();
            }catch(IOException)
            {
                throw new TestDataSetupException("Please close the testdata.xlsx file if open.");
            }
        }

        public DataSet testDataWorkBook { get; private set; }

        public static string TestDataPath
        {
            get
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory;
                var filePath = Path.Combine(dir.Substring(0, dir.LastIndexOf("Output")), "TestData");
                return filePath;
            }
        }

        public DataSet LoadExcel()
        {
            string fileName = TestDataPath + @"\testdata.xlsx";
            FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader;
            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            testDataWorkBook = excelReader.AsDataSet(new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true
                }
            });
            stream.Close();
            return testDataWorkBook;
        }

        public DataTable LoadCommonTestData()
        {
           return testDataWorkBook.Tables["CommonTestData"];
        }

        public DataTable LoadTestCaseData(string sheeName= "CRM")
        {
            return testDataWorkBook.Tables[sheeName];
        }

        public DataTable FilterRows(string searchTC = "SearchCustomer", string sheeName = "CRM")
        {
            DataTable dt = LoadTestCaseData(sheeName);
            try
            {
                return  dt.Rows
                                .Cast<DataRow>()
                                .Where(x => x["TestCase"].ToString().Equals(searchTC))
                                .CopyToDataTable();
            }
            catch (Exception)
            {
               // throw new TestDataSetupException($"Test setup wasn't done for TC: {search}");
            }
            return dt;
        }

        public int GetColumnNumber(DataTable dt, string columnName)
        {
            string[] columnNames = (from dc in dt.Columns.Cast<DataColumn>()
                                    select dc.ColumnName).ToArray();
            return Array.IndexOf(columnNames, columnNames.Where(x => x.Contains(columnName)).First());
        }
    }

    public class GetTestCaseSource
    {
        //public static DataRowCollection GetTestData()
        //{
        //    //string TestCaseName = TestContext.CurrentContext.Test.Name;
        //    //return new ReadExcel().FilterRows(TestCaseName).Rows;
        //    //return new ReadExcel().FilterRows().Rows;
        //}
    }

}
