﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REV8_Autoline.CoreFramework
{
    [AttributeUsage(AttributeTargets.Field)]
    public class TestObjectAttribute : Attribute
    {
        public string TestDataFiledName{get; set;}
        public string UIRefName { get; set; }

    }
}
