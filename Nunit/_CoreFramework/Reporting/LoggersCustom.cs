﻿using IVCDriveTests.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;

namespace REV8_Autoline.CoreFramework
{
    class LoggersCustom : WindowTestBase
    {
        TestBase testBase;
        public LoggersCustom(TestBase testBase)
        {
            this.testBase = testBase;
        }

        public void LogTestStep(string testStepName, string testStepDescription = "", bool addScreenShot = false)
        {
            Logger.TestStep(testStepName, testStepDescription);
            ExtentBase.GetTargetTest().Info($"{testStepName} \n {testStepDescription}");
        }

        public void LogPassStep(string testStepName, string testStepDescription = "", bool addScreenShot = false)
        {
            Logger.TestStep(testStepName, testStepDescription);
            ExtentBase.LogPassStep($"{testStepName} \n {testStepDescription}");
        }

        public void LogFailStep(string testStepName, string testStepDescription = "", bool addScreenShot = true)
        {
            Logger.TestStep(testStepName, testStepDescription);
            ExtentBase.LogFailStep($"{testStepName} \n {testStepDescription}");
        }

        public void LogWarningStep(string testStepName, string testStepDescription = "", bool addScreenShot = true)
        {
            Logger.TestStep(testStepName, testStepDescription);
            ExtentBase.LogWarnStep($"{testStepName} \n {testStepDescription}");
        }
    }
}
