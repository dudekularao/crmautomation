using REV8_Autoline.CoreFramework;
using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using TestStack.White.ScreenObjects;

namespace REV8_Autoline.CoreFramework
{
    class ScreenShotUtils
    {
        private enum ScreenshotOf
        {
            KcmlApp,
            Desktop,
            None
        }

        //public static void AddScreenshotToReport(string stepName = "")
        //{
        //    if (WindowTestBase.kcmlAppScreen == null)
        //        return;
        //    string screenshotFolder = Path.Combine(ExtentBase.extentReportPath, "Screenshots");
        //    if (!Directory.Exists(screenshotFolder))
        //        Directory.CreateDirectory(screenshotFolder);
        //    string path = $"{Path.Combine(ExtentBase.extentReportPath, "Screenshots")}/{DateTime.Now.ToString("yyyyMMddHHMMss")}";

        //    try
        //    {
        //        WindowTestBase.kcmlAppScreen.TakeScreenshot(path);
        //        byte[] imageArray = File.ReadAllBytes($"{path}.jpg");
        //        string base64 = Convert.ToBase64String(imageArray);
        //        string htmlBase64 = string.Format("<br/><a href='data:image/gif;base64,{0}' data-featherlight='image'><img style='width:250px' class='report-img' src='data:image/gif;base64,{0}' alt=Click on the image/></a>", base64);
        //        ExtentBase.LogPassStep($"{stepName} \n {htmlBase64}");
        //    }
        //    catch (Exception)
        //    { }
        //}

        public static string GetScreenShotAsBase64(bool flag)
        {
            if (!flag || ConfigSetUp.TurnOffAllScreenshots.ToLower().Equals("true"))
                return string.Empty;            
            string screenshotFolder = Path.Combine(ExtentBase.extentReportPath, "Screenshots");
            if (!Directory.Exists(screenshotFolder))
                Directory.CreateDirectory(screenshotFolder);
            string path = $"{Path.Combine(ExtentBase.extentReportPath, "Screenshots")}/{DateTime.Now.ToString("yyyyMMddHHMMss")}";

            try
            {
                WindowTestBase.kcmlAppScreen.TakeScreenshot(path);
                byte[] imageArray = File.ReadAllBytes($"{path}.jpg");
                string base64 = Convert.ToBase64String(imageArray);
                return string.Format("<br/><a href='data:image/gif;base64,{0}' data-featherlight='image'><img style='width:250px' class='report-img' src='data:image/gif;base64,{0}' alt=Click on the image/></a>", base64);
            }
            catch (Exception)
            { return string.Empty; }
        }

        public static string ConvertFileToBase64(string imgPath)
        {
            try
            {
                byte[] imageArray = File.ReadAllBytes(imgPath);
                string base64 = Convert.ToBase64String(imageArray);
                return string.Format("<br/><a href='data:image/gif;base64,{0}' data-featherlight='image'><img style='width:250px' class='report-img' src='data:image/gif;base64,{0}' alt=Click on the image/></a>", base64);
            }
            catch (Exception)
            { return string.Empty; }
        }

        //public static void AddScreenshotToReport<T>(T currentWindow, string stepName = "") where T : KcmlAppScreen
        //{
        //    string screenshotFolder = Path.Combine(ExtentBase.extentReportPath, "Screenshots");
        //    if (!Directory.Exists(screenshotFolder))
        //        Directory.CreateDirectory(screenshotFolder);
        //    string path = $"{Path.Combine(ExtentBase.extentReportPath, "Screenshots")}/{DateTime.Now.ToString("yyyyMMddHHMMss")}";
        //    try
        //    {
        //        currentWindow.TakeScreenshot(path);
        //        byte[] imageArray = File.ReadAllBytes($"{path}.jpg");
        //        string base64 = Convert.ToBase64String(imageArray);
        //        string htmlBase64 = string.Format("<br/><a href='data:image/gif;base64,{0}' data-featherlight='image'><img style='width:250px' class='report-img' src='data:image/gif;base64,{0}' alt=Click on the image/></a>", base64);
        //        ExtentBase.LogPassStep($"{stepName} \n {htmlBase64}");
        //    }
        //    catch (Exception)
        //    { ExtentBase.GetTargetTest().Warning($"Unable to capture for step :{stepName}"); }
        //}

    }
}