﻿using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AventStack.ExtentReports.Reporter;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using REV8_Autoline.CoreFramework;

namespace REV8_Autoline.CoreFramework
{
    /// <summary>
    /// Contains all the functionalities relating to initialization of 
    /// Extent Reports
    /// </summary>
    class ExtentBase
    {
        /// <summary>
        /// Making this private to maintain a SingleTon Instance
        /// </summary>
        private ExtentBase() { }

        private static ExtentReports extentReports;
        private static ExtentHtmlReporter htmlReporter;
        private static readonly Object objToCreateExtent = new Object();

        public static string ExecutionStartedAt { get; set; }
        /// <summary>
        /// This allows each thread to have an independent ExtentTest
        /// </summary>
        [ThreadStatic]
        private static ExtentTest extentParentTest;

        /// <summary>
        /// Read-only ExtentReport Instance
        /// </summary>
        private static ExtentReports extentInstance
        {
            get
            {
                lock (objToCreateExtent)
                {
                    if (extentReports == null)
                    {
                        CreateSingletonInstance();
                    }
                    return extentReports;
                }
            }
        }

        /// <summary>
        /// return Extent Report .html Path
        /// </summary>
        public static string extentReportPath
        {
            get
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory;
                var filePath = Path.Combine(dir.Substring(0, dir.LastIndexOf("Output")), "TestReports");
                Directory.CreateDirectory(filePath);
                return filePath;
            }
        }

        public static string extentConfigXML
        {
            get
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory;
                return Path.Combine(dir.Substring(0, dir.LastIndexOf("Output")), "extent-config.xml");

            }
        }
        private static ExtentReports CreateSingletonInstance()
        {
            ExecutionStartedAt = DateTime.Now.ToString();
            htmlReporter = new ExtentHtmlReporter(Path.Combine(extentReportPath, "htmlfile.html"));
            htmlReporter.LoadConfig(extentConfigXML);
            extentReports = new ExtentReports();
            extentReports.AttachReporter(htmlReporter);
            extentReports.AddSystemInfo("Project", "CDK Global");
            extentReports.AddSystemInfo("OS", Environment.OSVersion.ToString());
            extentReports.AddSystemInfo("HostID", System.Net.Dns.GetHostName());
            extentReports.AddSystemInfo("Test Pack", "CRM");
            return extentReports;
        }

        /// <summary>
        ///  to flush the testcase events to .html file 
        /// </summary>
        public static void FlushExtent()
        {
            extentInstance.Flush();
        }

        /// <summary>
        /// To Create a testCase under the .html testcase view. 
        /// This method is Thread Synchronized
        /// </summary>
        /// <param name="testName">DesiredTestCaseName</param>
        /// <param name="category">Any Category Tag</param>
        /// <param name="description">Test Description</param>
        /// <returns>the currentExtentTestCase Instance</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static ExtentTest CreateTest(string testName, string category = null, string description = null)
        {
            extentParentTest = extentInstance.CreateTest(testName, description);
            ExtentBase.GetTargetTest().AssignCategory("Total Tests");
            if (!(category == null))
                extentParentTest.AssignCategory(category);
            return extentParentTest;
        }

        /// <summary>
        /// returns the currentTestCase running on the current Thread
        /// Which can be used to log the test events 
        /// </summary>
        /// <returns>the currentExtentTestCase Instance</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static ExtentTest GetTargetTest()
        {
            return extentParentTest;
        }

        public static void LogPassStep(string testStepName, string testStepDescription = "", bool addScreenShot = false)
        {
            string screenshot = ScreenShotUtils.GetScreenShotAsBase64(addScreenShot);
            GetTargetTest().Pass($"{testStepName} \n {testStepDescription} \n {screenshot}");
        }

        public static void LogFailStep(string testStepName, string testStepDescription = "", bool addScreenShot = true)
        {
            string screenshot = ScreenShotUtils.GetScreenShotAsBase64(addScreenShot);
            GetTargetTest().Fail($"{testStepName} \n {testStepDescription} \n {screenshot}");
        }

        public static void LogInfoStep(string testStepName, string testStepDescription = "", bool addScreenShot = false)
        {
            string screenshot = ScreenShotUtils.GetScreenShotAsBase64(addScreenShot);
            GetTargetTest().Info($"{testStepName} \n {testStepDescription} \n {screenshot}");
        }

        public static void LogWarnStep(string testStepName, string testStepDescription = "", bool addScreenShot = true)
        {
            string screenshot = ScreenShotUtils.GetScreenShotAsBase64(addScreenShot);
            GetTargetTest().Warning($"{testStepName} \n {testStepDescription} \n {screenshot}");
        }


    }
}

