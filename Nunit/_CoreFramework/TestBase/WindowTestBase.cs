﻿using IVCDriveTests.Common;
using NUnit.Framework;
using REV8_Autoline._WindowObjects.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.ScreenObjects;


namespace REV8_Autoline.CoreFramework
{

    public class WindowTestBase : TestBase
    {
        [ThreadStatic]
        public static KcmlAppScreen kcmlAppScreen;

        public static DataTable CommonTestData ;
        public ConfigSetUp Config;
        public static ReadExcel readExcel;
        public static DataSet TestDataWorkBook;
        public DataTable TestData;


        //[OneTimeSetUp]
        public void OneTimeSetUp()
        {
        }
        //[OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [SetUp]
        public void TestSetUp()
        {
            var TestName = TestContext.CurrentContext.Test.Name;
            Config = new ConfigSetUp();
            readExcel = new ReadExcel();
            TestDataWorkBook = readExcel.testDataWorkBook;
            CommonTestData = readExcel.LoadCommonTestData();

            ExtentBase.CreateTest(TestName);
            ExtentBase.GetTargetTest().Pass("Test started");
        }

        [TearDown]
        public void TestTearDown()
        {
            while (ClickOnExitButton()) ;

            if (TestContext.CurrentContext.Result.Status == TestStatus.Failed)
                ExtentBase.LogFailStep("Test Failed");
            else
                ExtentBase.LogPassStep("Test Passed");
            ExtentBase.FlushExtent();
        }

        public bool ClickOnExitButton()
        {
            try
            {
                var kcmlLogin = Get<ModuleMenuWindow>();
                return kcmlLogin.ExitKcmlApp();
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected void LoginToKCML()
        {
            //Login(Config.ServerURI, Config.ServiceName, Config.LoginUserName, Config.LoginPassword, Config.Kpath);
            Login(GetCommonData("host"), GetCommonData("pack"), GetCommonData("userid"), GetCommonData("password"), Config.Kpath);
            var kcmlLogin = Get<KCMLLoginWindow>();
            kcmlLogin.AcceptAlertOK();
            kcmlLogin.WaitForWindow("9304E Pack");
            kcmlLogin = Get<KCMLLoginWindow>();

            if (kcmlLogin.WindowTitle.Contains("9304E"))
                ExtentBase.LogPassStep("KCML Login", "Login Success");
            else
                ExtentBase.LogFailStep("KCML Login", "Login failed");

        }

        protected string KCMLLoginIn_SelectCompanyModuleMenuItem(string Module, string Company, string MenuItem)
        {
            KcmlLogin_SelectModuleCompany SelectModuleCompany = GetWindow<KcmlLogin_SelectModuleCompany>();
            SelectModuleCompany.SelectGivenModuleAndCompany(Module, Company);
            ExtentBase.LogPassStep($"{Module} login", $"Module Selected : {Module}, Company: {Company}, MenuItem:{MenuItem}");

            ModuleMenuWindow objCRMmenu = GetWindow<ModuleMenuWindow>();
            objCRMmenu.SelectCRMMenu(MenuItem);
            Logger.TestStep($"{Module} login", $"{Module} loaded successfully");
            objCRMmenu = GetWindow<ModuleMenuWindow>();
            return objCRMmenu.WindowTitle;
        }

        public virtual T GetWindow<T>(bool exact = false, int timeout = 30000) where T : KcmlAppScreen
        {
            kcmlAppScreen =  Get<T>(exact ,timeout);
            return (T)kcmlAppScreen;
        }
      

        protected string GetData(string keyName, int index = 0)
        {
            try
            {
                return TestData.Rows[index][keyName].ToString();
            }
            catch (Exception)
            {
                throw new TestDataSetupException($"{keyName} wasn't found in test data, sheet:{TestData.TableName}") ;
            }
        }

        protected string GetCommonData(string keyName, int index = 1)
        {
            try
            {
                return CommonTestData.Select($"TestDataKey = '{keyName}'")[0][index].ToString();
            }
            catch (Exception)
            {
                throw new TestDataSetupException($"{keyName} wasn't found in common test data");
            }
        }

    }

}
