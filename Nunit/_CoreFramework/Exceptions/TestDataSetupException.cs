﻿using System;
using System.Runtime.Serialization;

namespace REV8_Autoline.CoreFramework
{
    /// <summary>
    /// This is a simple sample exception for invalid test data
    /// If needed, can implement more
    /// </summary>
    [Serializable]
    internal class TestDataSetupException : Exception
    {
        public TestDataSetupException()
        {
        }

        public TestDataSetupException(string message) : base(message)
        {
        }

        public TestDataSetupException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TestDataSetupException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}