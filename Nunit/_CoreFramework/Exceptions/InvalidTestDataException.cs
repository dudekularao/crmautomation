﻿using System;
using System.Runtime.Serialization;

namespace REV8_Autoline.CoreFramework
{
    /// <summary>
    /// This is a simple sample exception for invalid test data
    /// If needed, can implement more
    /// </summary>
    [Serializable]
    internal class InvalidTestDataException : Exception
    {
        public InvalidTestDataException()
        {
        }

        public InvalidTestDataException(string message) : base(message)
        {
        }

        public InvalidTestDataException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidTestDataException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}