﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REV8_Autoline.CoreFramework
{
    /// <summary>
    /// This class is used to load app.config file
    /// </summary>
    public class ConfigSetUp
    {

        public string ServerURI
        {
            get
            {
                return GetValueFromAppConfig("server", "");
            }
        }

        public static string TurnOffAllScreenshots
        {
            get
            {
                return GetValueFromAppConfig("TurnOffAllScreenshots", "false");
            }
        }

        public static string ScreenshotOnFailure
        {
            get
            {
                return GetValueFromAppConfig("ScreenshotOnFailure", "true");
            }
        }

        public static string ScreenshotOnPass
        {
            get
            {
                return GetValueFromAppConfig("ScreenshotOnPass", "false");
            }
        }

        public string ServiceName
        {
            get
            {
                return GetValueFromAppConfig("service", "");
            }
        }

        public string Kpath
        {
            get
            {
                return GetValueFromAppConfig("kpath", "");
            }
        }

        public string LoginUserName
        {
            get
            {

                return GetValueFromAppConfig("loginusername", "kcc");
            }
        }

        public string LoginPassword
        {
            get
            {
                return GetValueFromAppConfig("loginpassword", "kcc123");
            }
        }

        public static string GetValueFromAppConfig(string KeyName, string defaultValue = "")
        {
            var valueFromConfig = string.Empty;
            try
            {

                 valueFromConfig = ConfigurationManager.AppSettings[KeyName];
            }
            catch (Exception)
            {
                Console.WriteLine($"{KeyName} isn't found in App.config file");
            }
            if (string.IsNullOrEmpty(valueFromConfig))
                valueFromConfig = defaultValue;
            return valueFromConfig;
        }
    }
}
