﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.WindowItems;

namespace REV8_Autoline.CoreFramework
{

    [Title("KCML Client")]
    [Title("System information")]
    [Title("9304E Pack")]
    public class KCMLLoginWindow : WindowBase
    {

        public KCMLLoginWindow(Window window, ScreenRepository screenRepository)
              : base(window, screenRepository) { }

        [ByName("Please select a service:")]
        public DropDownList serviceListDD;
        [ByHelpText("Rev8")]
        public TextBox serviceName;
        [ByName("No")]
        public Button btnNo;
        [ByName("OK")]
        public Button alertOK;

        public virtual void AcceptAlertOK()
        {
            HandleDialog("System information", "OK");
            WaitForExist(3000);
        }

        public virtual void SelectServiceName()
        {
            /*  objlogPack = Get<loginPack>();
              serviceListDD.Click();
              Wait();
              serviceName.Click();
              Wait();
              */
        }

        public virtual void AcceptAlertNo()
        {
            // alertNo.Click();            
        }

    }

}
