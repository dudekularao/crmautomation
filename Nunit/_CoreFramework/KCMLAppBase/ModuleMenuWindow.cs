﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.TabItems;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems.Actions;
using System.Threading;
using TestStack.White.UIItems.WindowStripControls;
using NUnit.Framework;
using TestStack.White.UIItems.TableItems;
using REV8_Autoline.CoreFramework;
namespace REV8_Autoline.CoreFramework
{

    [Title("CRM")]
    [Title("Point of sale")]
    [Title("Select")]
    [Title("9304")]
    public class ModuleMenuWindow : WindowBase
    {
        public ModuleMenuWindow(Window window, ScreenRepository screenRepository) : base(window, screenRepository) { }

        [TestObject(TestDataFiledName = "Menu")]
        [ByHelpText("Menu..list")]
        public ListBox CRMmenuList;

        [ByHelpText("Menu..list")]
        public ListBox ModuleMenuList;

        [ByName("OK")]
        public Button btnOK;

        [ByName("Exit")]
        public Button Exit;

        public virtual void SelectCRMMenu(string MenuItem)
        {
            CRMmenuList.Select2(MenuItem); //"My desktop"
            WaitForExist(3000);
            btnOK.Click();
            WaitForExist(5000);
        }

        public virtual void SelectModuleMenu(string MenuItem, string windowTitle = "")
        {
            if (!string.IsNullOrEmpty(windowTitle))
            {
                WaitForWindow(windowTitle);
            }
            ModuleMenuList.Select2(MenuItem); //"My desktop"
            WaitForExist(3000);
            btnOK.Click();
            WaitForExist(5000);
        }

        public virtual bool ExitKcmlApp()
        {
            if (Exit.Exist())
            {
                if (WindowTitle.Contains("9304"))
                {
                    Exit.Click();
                    return false;
                }
                Exit.Click();
                WaitForExist(3000);
                return true;
            }
            else
                return false;

        }
    }
}
