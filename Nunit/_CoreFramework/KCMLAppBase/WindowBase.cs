﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace REV8_Autoline.CoreFramework
{
    public class WindowBase : KcmlAppScreen
    {
        public WindowBase(Window window, ScreenRepository screenRepository)
            : base(window, screenRepository) { }

        public virtual T GetWindow<T>(bool exact = false, bool wait = true, int timeout = 30000) where T : KcmlAppScreen
        {
            WindowTestBase.kcmlAppScreen = Get<T>(exact, wait, timeout);
            return (T)WindowTestBase.kcmlAppScreen;
        }
    }

    [Title("9304E Pack")]
    public class KcmlLogin_SelectModuleCompany : WindowBase
    {
        public KcmlLogin_SelectModuleCompany(Window window, ScreenRepository screenRepository)
            : base(window, screenRepository) { }
        [ByHelpText("module_list..list")]
        public ListBox moduleList;
        [ByHelpText("company_list..list")]
        public ListBox companyList;
        [ByName("OK")]
        public Button btnOK;
        //[ByHelpText("Menu..list")]
        //public ListBox CRMmenuList;
       

        public virtual void SelectGivenModuleAndCompany(string ModuleName, string CompanyName)
        {
            moduleList.Select2(ModuleName);
            WaitForExist(1000);            
            companyList.Select2(CompanyName);
            WaitForExist(1000);
            btnOK.Click();
            WaitForExist(3000);
        }

        public virtual void SelectGivenModuleAndCompany(string ModuleName, int CompanyNameIndex = 0)
        {
            moduleList.Select2(ModuleName);
            WaitForExist(1000);
            companyList.Select(CompanyNameIndex);               
            WaitForExist(1000);
            btnOK.Click();
            WaitForExist(3000);
        }

        public virtual void SelectSubModule(string ModuleName, string subModuleToSelect)
        {
            
            //GetKcmlWindow(ModuleName).Get
        }
    }

}
